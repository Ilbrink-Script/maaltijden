<?php

use App\Http\Controllers\UnitController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Auth logged in user route
 */
Route::get('user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {

    /**
     * Users module
     */
    Route::group(['prefix' => 'users'], function () {
        Route::get('', [UserController::class, 'index']);
        Route::post('', [UserController::class, 'store']);

        Route::get('/{user}', [UserController::class, 'show']);
        Route::put('/{user}', [UserController::class, 'update']);
        Route::delete('/{user}', [UserController::class, 'destroy']);
    });

    /**
     * App routes
     */
    Route::group(['prefix' => 'units'], function () {
        Route::get('', [UnitController::class, 'index']);
        Route::post('store', [UnitController::class, 'store']);

        Route::get('{unit}/edit', [UnitController::class, 'show']);
        Route::put('{unit}/update', [UnitController::class, 'update']);
        Route::delete('{unit}/delete', [UnitController::class, 'destroy']);
    });
});
