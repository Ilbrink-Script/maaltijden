<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supply extends Model
{
    use HasFactory;

    /**
     * Get the user this supply belongs to
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the ingredient this supply belongs to
     */
    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class);
    }
}
