const apiEndPoint = '/api/users';

export default {
    index: (params) => axios.get(apiEndPoint, { params: params }),
    show: (id) => axios.get(`${apiEndPoint}/${id}`),
    create: (data) => axios.post(apiEndPoint, data),
    update: (id, data) => axios.put(`${apiEndPoint}/${id}`, data),
    delete: (id) => axios.delete(`${apiEndPoint}/${id}`)
}