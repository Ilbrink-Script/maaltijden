const mix = require('laravel-mix');

require('laravel-mix-artisan-serve');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/scss/app.scss', 'public/css', [
        //
    ])
    // laravel breeze ui stuff, probably remove soon tm
    // /*
    .postCss('resources/css/app.css', 'public/css', [
        //
        require('postcss-import'),
        require('tailwindcss'),
        require('autoprefixer'),
    ])
    // */
    .copyDirectory(['resources/images'], 'public/images')
    .vue()
    // .serve({
    //     // host: '127.0.0.1',
    //     // port: '3000',
    // })
    .disableSuccessNotifications();
