<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    use HasFactory;

    /**
     * Get the category that owns this ingredient
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the meals that own this ingredient
     */
    public function meals()
    {
        return $this->belongsToMany(Meal::class);
    }

    /**
     * Get the unit for this ingredient
     */
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }
}
