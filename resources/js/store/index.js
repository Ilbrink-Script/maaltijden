import axios from 'axios';
import Vue from 'vue'
import Vuex from 'vuex'

// modules
import auth from './modules/auth'
import users from './modules/users'

// import { vm } from '../app'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        auth,
        users,
    },
    state: {
        units: [],
        ingredients: [],
        supplies: [],
        meals: [],

        authenticated: false,
        user: null,

        status: null,
    },
    mutations: {
        SET_USERS: (state, users) => state.users = users,
    },
    actions: {
        fetchUsers: async ({ commit }) => {
            return axios.get("/api/users")
                .then(response => {
                    commit('SET_USERS', response.data)
                }).catch(error => {
                    console.error('Failed to fetch users', error)
                })
        },

        fetchUser: async ({ }, id) => {
            return axios.get(`/api/users/${id}`)
        }

        
    },
    getters: {
        getUnit: state => id => state.units.find(unit => unit.id === id),
        getUserById: state => id => state.users.find(user => user.id === id),
        /* getAdmins: state => state.users.filter(user => user.admin),*/
    },
})

export default store