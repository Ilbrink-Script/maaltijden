<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Groente',
            'Fruit', 
            'Smeer- en bereidingsvetten',
            'Zuivel',
            'Noten',
            'Vis, peulvruchten, vlees, ei',
            'Brood, graanproducten, aardappelen',
            'Dranken',
        ];

        foreach ($categories as $category) {
            Category::create([
                'title' => $category,
            ]);
        }
    }
}
