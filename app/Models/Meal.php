<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    use HasFactory;

    /**
     * Get the ingredients that own this meal
     */
    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class);
    }
}
