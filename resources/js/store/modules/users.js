import userAPI from '../../api/users'
export default {
    namespaced: true,

    state: {
        users: [],
        users_loading: false,
        user: {},
        user_loading: false,
    },

    mutations: {
        SET_USERS: (state, users) => state.users = users,
        SET_USERS_LOADING: (state, loading) => state.users_loading = loading,
        SET_USER: (state, user) => state.user = user,
        SET_USER_LOADING: (state, loading) => state.user_loading = loading,
    },

    actions: {
        FETCH_USERS({ commit }, params = {}) {
            commit('SET_USERS_LOADING', true)
            return userAPI.index(params)
                .then(response => commit('SET_USERS', response.data))
                .catch(error => console.log('error fetching users', error))
                .finally(() => commit('SET_USERS_LOADING', false));
        },

        FETCH_USER({ commit }, id) {
            commit('SET_USER_LOADING', true)
            return userAPI.show(id)
                .then(response => { commit('SET_USER', response.data) })
                .catch(error => console.log('error fetching user', error))
                .finally(() => commit('SET_USER_LOADING', false))
        },

        ADD_USER({ commit }, data) {
            commit('SET_USER_LOADING', true)
            return userAPI.create(data)
                .then(response => commit('SET_USER', response.data))
                .catch(error => console.log('error adding user', error))
                .finally(() => commit('SET_USER_LOADING', false))
        },

        EDIT_USER({ commit }, { id, data }) {
            commit('SET_USER_LOADING', true)
            return userAPI.update(id, data)
                .then(response => commit('SET_USER', response.data))
                .finally(() => commit('SET_USER_LOADING', false))
        },

        DELETE_USER({ commit }, id) {
            return userAPI.delete(id)
                .then(response => commit('SET_USER', response.data))
                .catch(error => console.log('error deleting user', error));
        },

        getUsers: ({ dispatch, state }, { params = {}, forceFetch = false }) => {
            if (!state.users.length || forceFetch) dispatch('FETCH_USERS', params)

            return state.users;
        },
        getUserById: ({ state, dispatch }, { id, forceFetch = false }) => {
            if (state.user.id !== id || forceFetch) dispatch('FETCH_USER', id)

            return state.user;
        },
    },
}