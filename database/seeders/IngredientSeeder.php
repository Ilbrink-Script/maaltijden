<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Ingredient;
use App\Models\Unit;
use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ingredients = [
            'Groente' => [
                'g' => [
                    'Spinazie',
                    'Sla',
                    'Tomaten',
                    'Sperziebonen',
                    'Rode bieten',
                    'Rode kool',
                    'Tuinbonen',
                    'Wortels',
                    'Prei',
                    'Champignons',
                ],
            ],
            'Fruit' => [
                'x' => [
                    'Zoete appels',
                    'Zure appels',
                    'Peren',
                    'Bananen',
                    'Mango',
                    'Kiwi',
                    'Avocado',
                ],
                'g' => [
                    'Aardbeien',
                ],
            ],
            'Smeer- en bereidingsvetten' => [
                'g' => [
                    'Margarine',
                    'Roomboter',
                    'Reuzel',
                ],
                'l'  => [
                    'Olijfolie',
                    'Zonnebloemolie',
                ],
            ],
            'Zuivel' => [
                'l' => [
                    'Magere yoghurt',
                    'Volle yoghurt',
                    'Vanille vla',
                    'Turkse yoghurt',
                    'Kaas',
                ],
            ],
            'Noten' => [
                'g' => [
                    'Pinda\'s',
                    'Cashewnoten gezouten',
                    'Cashewnoten ongezouten',
                    'Japanse mix',
                ],
            ],
            'Vis, peulvruchten, vlees, ei' => [
                'x' => [
                    'Eieren',
                ],
                'g' => [
                    'Witte bonen',
                    'Bruine bonen',
                    'Kikkererwten',
                    'Zwarte bonen',
                    'Kapucijners',
                    'Lima bonen',
                    'Linzen',
                    'Spliterwten',
                    'Soja bonen',
                    'Kidneybonen',
                ],
            ],
            'Brood, graanproducten, aardappelen' => [
                'g' => [
                    'Volkoren brood',
                    'Meergranen brood',
                    'Tijgerbrood',
                    'Witbrood',
                    'Tarwebrood',
                    'Bloem',
                    'Meel',
                    'Aardappelen (kruimig)',
                    'Aardappelen (vastkokend)',
                ],
            ],
            'Dranken' => [
                'l' => [
                    'Water',
                    'Zwarte thee',
                    'Groene thee',
                    'Kruidenthee',
                    'Koffie',
                    'Bier',
                    'Rode wijn',
                    'Witte wijn',
                    'Whiskey',
                    'Tequila',
                ],
            ],
        ];

        $units = Unit::all();

        // category
        foreach ($ingredients as $cat => $unitsProducts) {
            $category = Category::where('title', $cat)->first();

            // units
            foreach ($unitsProducts as $u => $products) {
                $unit = $units->first(function ($val, $key) use ($u) {
                    return $val->short === $u;
                });

                // products
                foreach ($products as $product) {
                    Ingredient::factory()->create([
                        'category_id' => $category->id,
                        'unit_id' => $unit->id,
                        'title' => $product,
                    ]);
                }
            }
        }
    }
}
