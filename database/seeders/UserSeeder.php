<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'name' => 'Paul Ilbrink',
            'email' => 'paul@example.com',
            'password' => bcrypt('maaltijden'),
            'admin' => true,
        ]);

        // User::factory()->count(10)->create();
    }
}
