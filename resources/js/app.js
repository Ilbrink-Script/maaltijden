require('./bootstrap');

// something from Breeze, so remove at some point
// require('alpinejs');

import Vue from 'vue'

import Buefy from 'buefy'
Vue.use(Buefy, {
    // defaultIconPack: 'fa' 
    // defaultFieldLabelPosition: 'on-border'
})

import store from './store'
import router from './router'

import App from './App.vue'

// the Vue instance
let vm;
store.dispatch('auth/me').then(() => {
    vm = new Vue({
        el: "#app",
        router,
        store,
        render: h => h(App)
    })
});

export { vm }