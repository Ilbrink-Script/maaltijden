<?php

namespace Database\Seeders;

use App\Models\Unit;
use Illuminate\Database\Seeder;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = [
            ['title' => 'gram', 'short' =>'g'],
            ['title' => 'liter', 'short' => 'l'],
            ['title' => 'stuks', 'short' => 'x'],
            
        ];

        foreach ($units as $unit) {
            Unit::factory()->create($unit);
        }
    }
}
