import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store';


import routes from './routes';

Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    routes
});

// global guards
router.beforeEach(function (to, from, next) {
    let approved = true;
    let redirect;

    switch (to.meta.middleware) {
        case 'auth':
            approved = store.getters['auth/authenticated'];
            if (!approved) redirect = { name: 'auth.login' };
            break;
        case 'guest':
            approved = !store.getters['auth/authenticated'];
            break;
        case 'admin':
            approved = store.getters['auth/authenticated'] && store.getters['auth/user'].admin;
        default:
            // whatever, go ahead
    }

    if (redirect) next(redirect)
    else if (!approved) next(false)
    else next()
});

// global route helper functions minxin's
Vue.mixin({
    methods: {
        routeNameExists(name) {
            return this.$router.resolve({ name: name }).resolved.matched.length > 0;
        },
        routeHref(name) {
            if (!this.routeNameExists(name)) return false;

            return this.$router.resolve({ name: name }).href;
        }
    }
})

export default router;