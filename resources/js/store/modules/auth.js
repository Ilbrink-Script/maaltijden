export default {
    namespaced: true,

    state: {
        authenticated: false,
        user: null
    },

    mutations: {
        SET_AUTHENTICATED(state, value) {
            state.authenticated = value
        },

        SET_USER(state, value) {
            state.user = value
        }
    },

    actions: {
        async login({ dispatch }, credentials) {
            await axios.get('/sanctum/csrf-cookie')
            await axios.post('/login', credentials)

            return dispatch('me')
        },

        async logout({ dispatch }) {
            await axios.post('/logout')

            return dispatch('me')
        },

        async register({ dispatch }, data) {
            await axios.get('/sanctum/csrf-cookie')
            await axios.post('/register', data)

            return dispatch('me')
        },

        async passwordRequest({  }, data) {
            await axios.get('/sanctum/csrf-cookie')
            return await axios.post('/forgot-password', data)
        },
        
        async passwordReset({  }, data) {
            await axios.get('/sanctum/csrf-cookie')
            return await axios.post('/reset-password', data)
        },

        me({ commit }) {
            return axios.get('/api/user').then(response => {
                commit('SET_AUTHENTICATED', !!response.data)
                commit('SET_USER', response.data || null)
            }).catch(error => {
                commit('SET_AUTHENTICATED', false)
                commit('SET_USER', null)
            })
        }
    },

    getters: {
        authenticated(state) {
            return state.authenticated
        },

        user(state) {
            return state.user
        },
    },
}