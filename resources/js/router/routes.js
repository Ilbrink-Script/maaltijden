import store from '../store'

import Home from '../components/Home'

import Login from '../components/auth/Login'
import Register from '../components/auth/Register'
import PasswordRequest from '../components/auth/PasswordRequest'
import PasswordReset from '../components/auth/PasswordReset'

import UserIndex from '../components/users/Index'
import UserShow from '../components/users/Show'

import UnitIndex from '../components/units/Index'
import UnitShow from './../components/unit/Show'

import IngredientIndex from '../components/ingredients/Index'
import SupplyIndex from '../components/supplies/Index'
import MealIndex from '../components/meals/Index'

const routes = [
    { path: '/', component: Home, name: 'home', title: 'Home', meta: {} },

    /**
     * Auth module
     */
    { path: '/login', component: Login, name: 'auth.login', title: 'Login', meta: { middleware: 'guest' }, },
    { path: '/register', component: Register, name: 'auth.register', title: 'Register', meta: { middleware: 'guest' }, },
    { path: '/forgot-password', component: PasswordRequest, name: 'auth.password.request', title: 'Request password', meta: { middleware: 'guest' }, },
    { path: '/reset-password/:token', component: PasswordReset, name: 'auth.password.reset', title: 'Reset password', meta: { middleware: 'guest' }, props: true },

    /**
     * Users module
     */
    { path: '/users', component: UserIndex, name: 'users.index', title: 'Users', meta: { middleware: 'admin' }, },
    { path: '/users/:id', component: UserShow, name: 'users.show', title: 'User Show', meta: { middleware: 'auth' }, },
    /* props: (route) => {
        const id = Number.parseInt(route.params.id)
        if (Number.isNaN(id)) return 0
        return { id }
    }
},*/

    { path: '/units', component: UnitIndex, name: 'units.index', title: 'Units', meta: { middleware: 'auth' } },
    { path: '/ingredients', component: IngredientIndex, name: 'ingredients.index', title: 'Ingredients', meta: { middleware: 'auth' } },
    { path: '/supplies', component: SupplyIndex, name: 'supplies.index', title: 'Supplies', meta: { middleware: 'auth' } },
    { path: '/meals', component: MealIndex, name: 'meals.index', title: 'Meal', meta: { middleware: 'auth' } },
]

export default routes;